package lk.starpasspro.api.questions.configs;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import lk.starpasspro.api.questions.handlers.ObservableReturnValueHandler;

@Configuration
@ComponentScan(basePackages = { "lk.starpasspro.*" })
public class WebConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
		returnValueHandlers.add(new ObservableReturnValueHandler());
	}
}
