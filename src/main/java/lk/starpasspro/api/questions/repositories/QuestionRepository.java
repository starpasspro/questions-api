package lk.starpasspro.api.questions.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import lk.starpasspro.api.questions.models.BaseQuestion;

public interface QuestionRepository extends MongoRepository<BaseQuestion, String> {

}
