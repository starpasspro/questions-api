package lk.starpasspro.api.questions.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import lk.starpasspro.api.questions.models.AccountCredential;

public interface AccountCredentialsRepository extends MongoRepository<AccountCredential, String> {
	@Query("{'username':?0}")
	AccountCredential findByUsername(String username);
}
