package lk.starpasspro.api.questions.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import lk.starpasspro.api.questions.dtos.BaseQuestionDto;
import lk.starpasspro.api.questions.dtos.MatchingQuestionDto;
import lk.starpasspro.api.questions.dtos.MatchingQuestionOutputDto;
import lk.starpasspro.api.questions.dtos.MultipleChoiceQuestionDto;
import lk.starpasspro.api.questions.dtos.MultipleChoiceQuestionOutputDto;
import lk.starpasspro.api.questions.dtos.QuestionCardDto;
import lk.starpasspro.api.questions.dtos.QuestionCardOutputDto;
import lk.starpasspro.api.questions.dtos.ShortAnswerQuestionDto;
import lk.starpasspro.api.questions.dtos.ShortAnswerQuestionOutputDto;
import lk.starpasspro.api.questions.models.BaseQuestion;
import lk.starpasspro.api.questions.models.MatchingQuestion;
import lk.starpasspro.api.questions.models.MultipleChoiceQuestion;
import lk.starpasspro.api.questions.models.QuestionCard;
import lk.starpasspro.api.questions.models.ShortAnswerQuestion;

@Mapper(componentModel = "spring")
public abstract class QuestionMapper {

	@Mapping(target = "id", ignore = true)
	protected abstract MultipleChoiceQuestion toMultipleChoiceQuestion(
			MultipleChoiceQuestionDto multipleChoiceQuestionDto);

	@Mapping(target = "id", ignore = true)
	protected abstract ShortAnswerQuestion toShortAnswerQuestion(ShortAnswerQuestionDto shortAnswerQuestionDto);

	@Mapping(target = "id", ignore = true)
	protected abstract MatchingQuestion toMatchingQuestion(MatchingQuestionDto matchingQuestionDto);

	@Mapping(target = "id", ignore = true)
	protected abstract QuestionCard toQuestionCard(QuestionCardDto questionCardDto);

	protected abstract MultipleChoiceQuestionDto toMultipleChoiceQuestionDto(
			MultipleChoiceQuestion multipleChoiceQuestion);

	protected abstract ShortAnswerQuestionDto toShortAnswerQuestionDto(ShortAnswerQuestion shortAnswerQuestion);

	protected abstract MatchingQuestionDto toMatchingQuestionDto(MatchingQuestion matchingQuestion);

	protected abstract QuestionCardDto toQuestionCardDto(QuestionCard questionCard);

	@Mappings({ @Mapping(target = "createdAt", expression = "java(multipleChoiceQuestion.getCreatedAt().toString())"),
			@Mapping(target = "updatedAt", expression = "java(multipleChoiceQuestion.getUpdatedAt().toString())") })
	protected abstract MultipleChoiceQuestionOutputDto toMultipleChoiceQuestionOutputDto(
			MultipleChoiceQuestion multipleChoiceQuestion);

	@Mappings({ @Mapping(target = "createdAt", expression = "java(shortAnswerQuestion.getCreatedAt().toString())"),
			@Mapping(target = "updatedAt", expression = "java(shortAnswerQuestion.getUpdatedAt().toString())") })
	protected abstract ShortAnswerQuestionOutputDto toShortAnswerQuestionOutputDto(
			ShortAnswerQuestion shortAnswerQuestion);

	@Mappings({ @Mapping(target = "createdAt", expression = "java(matchingQuestion.getCreatedAt().toString())"),
			@Mapping(target = "updatedAt", expression = "java(matchingQuestion.getUpdatedAt().toString())") })
	protected abstract MatchingQuestionOutputDto toMatchingQuestionOutputDto(MatchingQuestion matchingQuestion);

	@Mappings({ @Mapping(target = "createdAt", expression = "java(questionCard.getCreatedAt().toString())"),
			@Mapping(target = "updatedAt", expression = "java(questionCard.getUpdatedAt().toString())") })
	protected abstract QuestionCardOutputDto toQuestionCardOutputDto(QuestionCard questionCard);

	public BaseQuestion toBaseQuestion(BaseQuestionDto baseQuestionDto) {
		if (baseQuestionDto instanceof MultipleChoiceQuestionDto)
			return toMultipleChoiceQuestion((MultipleChoiceQuestionDto) baseQuestionDto);
		else if (baseQuestionDto instanceof ShortAnswerQuestionDto)
			return toShortAnswerQuestion((ShortAnswerQuestionDto) baseQuestionDto);
		else if (baseQuestionDto instanceof MatchingQuestionDto)
			return toMatchingQuestion((MatchingQuestionDto) baseQuestionDto);
		else if (baseQuestionDto instanceof QuestionCardDto)
			return toQuestionCard((QuestionCardDto) baseQuestionDto);
		else
			return null;
	}

	public BaseQuestionDto toBaseQuestionDto(BaseQuestion baseQuestion) {
		if (baseQuestion instanceof MultipleChoiceQuestion)
			return toMultipleChoiceQuestionDto((MultipleChoiceQuestion) baseQuestion);
		else if (baseQuestion instanceof ShortAnswerQuestion)
			return toShortAnswerQuestionDto((ShortAnswerQuestion) baseQuestion);
		else if (baseQuestion instanceof MatchingQuestion)
			return toMatchingQuestionDto((MatchingQuestion) baseQuestion);
		else if (baseQuestion instanceof QuestionCard)
			return toQuestionCardDto((QuestionCard) baseQuestion);
		else
			return null;
	}

	public BaseQuestionDto toBaseQuestionOutputDto(BaseQuestion baseQuestion) {
		if (baseQuestion instanceof MultipleChoiceQuestion)
			return toMultipleChoiceQuestionOutputDto((MultipleChoiceQuestion) baseQuestion);
		else if (baseQuestion instanceof ShortAnswerQuestion)
			return toShortAnswerQuestionOutputDto((ShortAnswerQuestion) baseQuestion);
		else if (baseQuestion instanceof MatchingQuestion)
			return toMatchingQuestionOutputDto((MatchingQuestion) baseQuestion);
		else if (baseQuestion instanceof QuestionCard)
			return toQuestionCardOutputDto((QuestionCard) baseQuestion);
		else
			return null;
	}

	public List<BaseQuestionDto> toBaseQuestionOutputDtos(List<BaseQuestion> baseQuestions) {
		return baseQuestions.stream().map(question -> toBaseQuestionOutputDto(question)).collect(Collectors.toList());
	};

}
