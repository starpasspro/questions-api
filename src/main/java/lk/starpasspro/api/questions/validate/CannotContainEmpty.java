package lk.starpasspro.api.questions.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CannotContainEmptyValidator.class)
public @interface CannotContainEmpty {
	String message() default "Cannot Contain empty values";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
