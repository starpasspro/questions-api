package lk.starpasspro.api.questions.validate;

import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CannotContainNullValidator implements ConstraintValidator<CannotContainNull, Set<?>> {

	@Override
	public void initialize(CannotContainNull constraintAnnotation) {

	}

	@Override
	public boolean isValid(Set<?> value, ConstraintValidatorContext context) {
		if (value == null)
			return false;

		return value.stream().allMatch(v -> !(v == null));
	}

}
