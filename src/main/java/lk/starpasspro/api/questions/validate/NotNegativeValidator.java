package lk.starpasspro.api.questions.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotNegativeValidator implements ConstraintValidator<NotNegative, Integer> {

	@Override
	public void initialize(NotNegative constraintAnnotation) {

	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		if (value == null)
			return false;

		return !(value < 0);
	}

}
