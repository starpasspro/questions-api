package lk.starpasspro.api.questions.validate;

import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CannotContainEmptyValidator implements ConstraintValidator<CannotContainEmpty, Set<?>> {

	@Override
	public void initialize(CannotContainEmpty constraintAnnotation) {

	}

	@Override
	public boolean isValid(Set<?> value, ConstraintValidatorContext context) {
		if (value == null)
			return false;

		return value.stream().allMatch(v -> !(v == null || v.equals("")));
	}

}
