package lk.starpasspro.api.questions.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.questions.dtos.BaseQuestionDto;
import lk.starpasspro.api.questions.mappers.QuestionMapper;
import lk.starpasspro.api.questions.models.BaseQuestion;
import lk.starpasspro.api.questions.repositories.QuestionRepository;
import lk.starpasspro.api.questions.services.QuestionService;
import lk.starpasspro.api.questions.translators.exceptions.ResourceNotFoundException;
import rx.Observable;

@Service
public class QuestionServiceImpl implements QuestionService {

	private static final Logger log = LoggerFactory.getLogger(QuestionService.class);

	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private QuestionMapper questionMapper;

	@Override
	public Observable<BaseQuestionDto> saveQuestion(BaseQuestionDto questionDto) {
		return save(questionMapper.toBaseQuestion(questionDto))
				.map(savedQuestion -> questionMapper.toBaseQuestionOutputDto(savedQuestion));
	}

	@Override
	public Observable<BaseQuestionDto> updateQuestion(String id, BaseQuestionDto questionDto) {
		return findOne(id).flatMap(fetchedQuestion -> {
			if (fetchedQuestion == null)
				throw new ResourceNotFoundException("No question found for update with Id : " + id);
			BeanUtils.copyProperties(questionDto, fetchedQuestion, "id", "createdAt", "updatedAt");
			return save(fetchedQuestion)
					.map(updatedQuestion -> questionMapper.toBaseQuestionOutputDto(updatedQuestion));
		});
	}

	@Override
	public Observable<BaseQuestionDto> findQuestion(String id) {
		return findOne(id).map(fetchedQuestion -> {
			if (fetchedQuestion == null)
				throw new ResourceNotFoundException("No question found with Id : " + id);
			return questionMapper.toBaseQuestionOutputDto(fetchedQuestion);
		});
	}

	@Override
	public Observable<List<BaseQuestionDto>> findAllQuestionsByIds(List<String> ids) {
		return findAll(ids).map(fetchedQuestions -> questionMapper.toBaseQuestionOutputDtos(fetchedQuestions));
	}

	@Override
	public Observable<List<BaseQuestionDto>> findAllQuestions() {
		return findAll().map(fetchedQuestions -> questionMapper.toBaseQuestionOutputDtos(fetchedQuestions));
	}

	@Override
	public Observable<Void> deleteQuestion(String id) {
		return findOne(id).flatMap(fetchedQuestion -> {
			if (fetchedQuestion == null)
				throw new ResourceNotFoundException("No question found for delete with Id : " + id);
			return delete(id);
		});
	}

	private Observable<BaseQuestion> save(BaseQuestion question) {
		return Observable.<BaseQuestion>create(sub -> {
			BaseQuestion savedQuestion = questionRepository.save(question);
			sub.onNext(savedQuestion);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Question was successfully saved."))
				.doOnError(e -> log.error("An ERROR occurred while saving the Question.", e));
	}

	private Observable<BaseQuestion> findOne(String id) {
		return Observable.<BaseQuestion>create(sub -> {
			BaseQuestion question = questionRepository.findOne(id);
			sub.onNext(question);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Question was successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Question.", e));
	}

	private Observable<List<BaseQuestion>> findAll(List<String> ids) {
		return Observable.<List<BaseQuestion>>create(sub -> {
			List<BaseQuestion> questions = (List<BaseQuestion>) questionRepository.findAll(ids);
			sub.onNext(questions);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Questions were successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Question.", e));
	}

	private Observable<List<BaseQuestion>> findAll() {
		return Observable.<List<BaseQuestion>>create(sub -> {
			List<BaseQuestion> questions = (List<BaseQuestion>) questionRepository.findAll();
			sub.onNext(questions);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Questions were successfully fetched."))
				.doOnError(e -> log.error("An ERROR occurred while fetching the Questions.", e));
	}

	private Observable<Void> delete(String id) {
		return Observable.<Void>create(sub -> {
			questionRepository.delete(id);
			sub.onNext(null);
			sub.onCompleted();
		}).doOnNext(p -> log.debug("Question was successfully deleted."))
				.doOnError(e -> log.error("An ERROR occurred while deleting the Question.", e));
	}

}
