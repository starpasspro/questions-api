package lk.starpasspro.api.questions.services;

import lk.starpasspro.api.questions.models.AccountCredential;

public interface AccountCredentialService {
	AccountCredential getCredential(String username);
}
