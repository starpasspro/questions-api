package lk.starpasspro.api.questions.services;

import java.util.List;

import lk.starpasspro.api.questions.dtos.BaseQuestionDto;
import rx.Observable;

public interface QuestionService {

	Observable<BaseQuestionDto> saveQuestion(BaseQuestionDto questionDto);

	Observable<BaseQuestionDto> updateQuestion(String id, BaseQuestionDto questionDto);

	Observable<BaseQuestionDto> findQuestion(String id);

	Observable<List<BaseQuestionDto>> findAllQuestionsByIds(List<String> ids);

	Observable<List<BaseQuestionDto>> findAllQuestions();

	Observable<Void> deleteQuestion(String id);

}
