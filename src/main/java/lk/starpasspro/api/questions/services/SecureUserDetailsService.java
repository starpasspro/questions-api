package lk.starpasspro.api.questions.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.questions.models.AccountCredential;
import lk.starpasspro.api.questions.security.SecureUserDetails;

@Service
public class SecureUserDetailsService implements UserDetailsService {

	@Autowired
	AccountCredentialService userCredentialService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AccountCredential userCredential = userCredentialService.getCredential(username);
		if (userCredential == null) {
			throw new UsernameNotFoundException(username);
		} else {
			UserDetails details = new SecureUserDetails(userCredential);
			return details;
		}
	}

}