package lk.starpasspro.api.questions.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lk.starpasspro.api.questions.models.AccountCredential;
import lk.starpasspro.api.questions.repositories.AccountCredentialsRepository;
import lk.starpasspro.api.questions.services.AccountCredentialService;

@Service
public class AccountCredentialServiceImpl implements AccountCredentialService {

	@Autowired
	private AccountCredentialsRepository accountCredentialsRepository;

	@Override
	public AccountCredential getCredential(String username) {
		return accountCredentialsRepository.findByUsername(username);
	}

}
