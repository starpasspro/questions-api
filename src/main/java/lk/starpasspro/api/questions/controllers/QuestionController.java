package lk.starpasspro.api.questions.controllers;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lk.starpasspro.api.questions.dtos.BaseQuestionDto;
import lk.starpasspro.api.questions.services.QuestionService;
import rx.Observable;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {

	private static Logger logger = LoggerFactory.getLogger(QuestionController.class);
	@Autowired
	private QuestionService questionService;

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<BaseQuestionDto> saveQuestion(@Validated @RequestBody BaseQuestionDto questionDto) {
		logger.info("Request to save Question " + questionDto.getType().name());
		return questionService.saveQuestion(questionDto);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<BaseQuestionDto> updateQuestion(@PathVariable String id,
			@Validated @RequestBody BaseQuestionDto questionDto) {
		logger.info("Request to update Question " + questionDto.getType().name() + " with id : " + id);
		return questionService.updateQuestion(id, questionDto);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<BaseQuestionDto> findQuestion(@PathVariable String id) {
		return questionService.findQuestion(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<List<BaseQuestionDto>> findAllQuestionsByIds(
			@RequestParam(name = "id", required = false) String[] ids) {
		if (ids != null && ids.length > 0)
			return questionService.findAllQuestionsByIds(Arrays.asList(ids));
		return questionService.findAllQuestions();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Observable<Void> deleteQuestion(@PathVariable String id) {
		return questionService.deleteQuestion(id);
	}

}
