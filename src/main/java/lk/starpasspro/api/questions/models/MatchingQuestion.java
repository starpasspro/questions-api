package lk.starpasspro.api.questions.models;

import java.util.List;

public class MatchingQuestion extends BaseQuestion {

	private List<LeftOption> leftOptions;
	private List<MatchingOption> rightOptions;

	public List<LeftOption> getLeftOptions() {
		return leftOptions;
	}

	public void setLeftOptions(List<LeftOption> leftOptions) {
		this.leftOptions = leftOptions;
	}

	public List<MatchingOption> getRightOptions() {
		return rightOptions;
	}

	public void setRightOptions(List<MatchingOption> rightOptions) {
		this.rightOptions = rightOptions;
	}

}
