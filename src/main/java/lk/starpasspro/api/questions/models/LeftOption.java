package lk.starpasspro.api.questions.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class LeftOption extends MatchingOption {

	@NotNull(message = "'rightOptionIndex' Cannot be null")
	@Min(value = 0, message = "'rightOptionIndex' Cannot be less than zero")
	private Integer rightOptionIndex;

	public Integer getRightOptionIndex() {
		return rightOptionIndex;
	}

	public void setRightOptionIndex(Integer rightOptionIndex) {
		this.rightOptionIndex = rightOptionIndex;
	}

}
