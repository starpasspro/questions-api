package lk.starpasspro.api.questions.models;

public class ShortAnswerQuestion extends BaseQuestion {

	private TextDisplay sampleCorrectAnswer;

	public TextDisplay getSampleCorrectAnswer() {
		return sampleCorrectAnswer;
	}

	public void setSampleCorrectAnswer(TextDisplay sampleCorrectAnswer) {
		this.sampleCorrectAnswer = sampleCorrectAnswer;
	}

}
