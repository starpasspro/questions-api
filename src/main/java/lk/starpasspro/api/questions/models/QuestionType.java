package lk.starpasspro.api.questions.models;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum QuestionType {
	MULTIPLE_CHOICE, SHORT_ANSWER, MATCHING, QUESTION_CARD;

	private static Map<String, QuestionType> TYPES_MAP = Stream.of(QuestionType.values())
			.collect(Collectors.toMap(s -> s.name(), Function.identity()));

	public static QuestionType fromString(String key) {
		QuestionType type = TYPES_MAP.get(key);

		if (type == null) {
			throw new IllegalArgumentException("\'" + key + "\' invalid question type value");
		}
		return type;
	}

}
