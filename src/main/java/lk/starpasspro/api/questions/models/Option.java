package lk.starpasspro.api.questions.models;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class Option {

	@NotNull(message = "'id' Cannot be null")
	@NotEmpty(message = "'id' Cannot be empty")
	private String id;
	
	@NotNull(message = "'label' Cannot be null")
	@NotEmpty(message = "'label' Cannot be empty")
	private String label;
	
	@Valid
	@NotNull(message = "'prompt' Cannot be null")
	private TextDisplay prompt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public TextDisplay getPrompt() {
		return prompt;
	}

	public void setPrompt(TextDisplay prompt) {
		this.prompt = prompt;
	}

}
