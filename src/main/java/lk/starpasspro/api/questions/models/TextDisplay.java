package lk.starpasspro.api.questions.models;

import javax.validation.constraints.NotNull;

public class TextDisplay {

	@NotNull(message = "'textValue' Cannot be null")
	private String textValue;

	@NotNull(message = "'imageValue' Cannot be null")
	private String imageValue;

	public String getTextValue() {
		return textValue;
	}

	public void setTextValue(String textValue) {
		this.textValue = textValue;
	}

	public String getImageValue() {
		return imageValue;
	}

	public void setImageValue(String imageValue) {
		this.imageValue = imageValue;
	}

}
