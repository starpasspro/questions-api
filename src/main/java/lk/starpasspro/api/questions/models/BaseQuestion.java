package lk.starpasspro.api.questions.models;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "questions")
public abstract class BaseQuestion {

	@Id
	protected String id;
	protected TextDisplay prompt;
	protected QuestionType type;
	protected int marks;
	protected int estimatedTime;
	protected List<String> tags;
	protected String owner;
	protected boolean shareableWithOtherApps;

	@CreatedDate
	protected DateTime createdAt;

	@LastModifiedDate
	protected DateTime updatedAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TextDisplay getPrompt() {
		return prompt;
	}

	public void setPrompt(TextDisplay prompt) {
		this.prompt = prompt;
	}

	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isShareableWithOtherApps() {
		return shareableWithOtherApps;
	}

	public void setShareableWithOtherApps(boolean shareableWithOtherApps) {
		this.shareableWithOtherApps = shareableWithOtherApps;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public DateTime getUpdatedAt() {
		return updatedAt;
	}

}
