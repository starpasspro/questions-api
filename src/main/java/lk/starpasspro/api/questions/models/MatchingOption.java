package lk.starpasspro.api.questions.models;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class MatchingOption {

	@NotNull(message = "'id' Cannot be null")
	@NotEmpty(message = "'id' Cannot be empty")
	protected String id;
	
	@Valid
	@NotNull(message = "'prompt' Cannot be null")
	protected TextDisplay prompt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TextDisplay getPrompt() {
		return prompt;
	}

	public void setPrompt(TextDisplay prompt) {
		this.prompt = prompt;
	}

}
