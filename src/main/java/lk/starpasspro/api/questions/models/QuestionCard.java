package lk.starpasspro.api.questions.models;

public class QuestionCard extends BaseQuestion {

	private TextDisplay answer;

	public TextDisplay getAnswer() {
		return answer;
	}

	public void setAnswer(TextDisplay answer) {
		this.answer = answer;
	}

}
