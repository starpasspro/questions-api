package lk.starpasspro.api.questions.models;

public class SuccessResult extends Result {
	private Object data;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
