package lk.starpasspro.api.questions.models;

import java.util.List;

public class MultipleChoiceQuestion extends BaseQuestion {

	private List<Option> options;
	private int correctAnswer;
	private TextDisplay explanation;

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public TextDisplay getExplanation() {
		return explanation;
	}

	public void setExplanation(TextDisplay explanation) {
		this.explanation = explanation;
	}

}
