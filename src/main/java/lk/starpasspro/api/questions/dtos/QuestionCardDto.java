package lk.starpasspro.api.questions.dtos;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lk.starpasspro.api.questions.models.TextDisplay;

public class QuestionCardDto extends BaseQuestionDto {

	@Valid
	@NotNull(message = "'answer' Cannot be null")
	private TextDisplay answer;

	public TextDisplay getAnswer() {
		return answer;
	}

	public void setAnswer(TextDisplay answer) {
		this.answer = answer;
	}

}
