package lk.starpasspro.api.questions.dtos;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lk.starpasspro.api.questions.models.TextDisplay;

public class ShortAnswerQuestionDto extends BaseQuestionDto {

	@Valid
	@NotNull(message = "'sampleCorrectAnswer' Cannot be null")
	private TextDisplay sampleCorrectAnswer;

	public TextDisplay getSampleCorrectAnswer() {
		return sampleCorrectAnswer;
	}

	public void setSampleCorrectAnswer(TextDisplay sampleCorrectAnswer) {
		this.sampleCorrectAnswer = sampleCorrectAnswer;
	}

}
