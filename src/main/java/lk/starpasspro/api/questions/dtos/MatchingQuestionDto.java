package lk.starpasspro.api.questions.dtos;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lk.starpasspro.api.questions.models.LeftOption;
import lk.starpasspro.api.questions.models.MatchingOption;
import lk.starpasspro.api.questions.validate.CannotContainEmpty;
import lk.starpasspro.api.questions.validate.CannotContainNull;

public class MatchingQuestionDto extends BaseQuestionDto {

	@Valid
	@NotNull(message = "'leftOptions' Cannot be null")
	@CannotContainNull(message = "'leftOptions' Cannot contain null")
	@CannotContainEmpty(message = "'leftOptions' Cannot contain empty values")
	private Set<LeftOption> leftOptions;

	@Valid
	@NotNull(message = "'rightOptions' Cannot be null")
	@CannotContainNull(message = "'rightOptions' Cannot contain null")
	@CannotContainEmpty(message = "'rightOptions' Cannot contain empty values")
	private Set<MatchingOption> rightOptions;

	public Set<LeftOption> getLeftOptions() {
		return leftOptions;
	}

	public void setLeftOptions(Set<LeftOption> leftOptions) {
		this.leftOptions = leftOptions;
	}

	public Set<MatchingOption> getRightOptions() {
		return rightOptions;
	}

	public void setRightOptions(Set<MatchingOption> rightOptions) {
		this.rightOptions = rightOptions;
	}

}
