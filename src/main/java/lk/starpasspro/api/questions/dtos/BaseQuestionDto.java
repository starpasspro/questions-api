package lk.starpasspro.api.questions.dtos;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lk.starpasspro.api.questions.models.QuestionType;
import lk.starpasspro.api.questions.models.TextDisplay;
import lk.starpasspro.api.questions.validate.CannotContainEmpty;
import lk.starpasspro.api.questions.validate.CannotContainNull;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", defaultImpl = BaseQuestionDto.class, visible = true)
@JsonSubTypes({ @Type(value = MultipleChoiceQuestionDto.class, name = "MULTIPLE_CHOICE"),
		@Type(value = ShortAnswerQuestionDto.class, name = "SHORT_ANSWER"),
		@Type(value = MatchingQuestionDto.class, name = "MATCHING"),
		@Type(value = QuestionCardDto.class, name = "QUESTION_CARD"),
		@Type(value = MultipleChoiceQuestionOutputDto.class, name = "MULTIPLE_CHOICE"),
		@Type(value = ShortAnswerQuestionOutputDto.class, name = "SHORT_ANSWER"),
		@Type(value = MatchingQuestionOutputDto.class, name = "MATCHING"),
		@Type(value = QuestionCardOutputDto.class, name = "QUESTION_CARD") })
public class BaseQuestionDto {

	@Valid
	@NotNull(message = "'prompt' Cannot be null")
	protected TextDisplay prompt;

	@NotNull(message = "'type' Cannot be null")
	protected QuestionType type;

	@NotNull(message = "'marks' Cannot be null")
	@Min(value = 0, message = "'marks' Cannot be less than zero")
	protected Integer marks;

	@NotNull(message = "'estimatedTime' Cannot be null")
	@Min(value = 0, message = "'estimatedTime' Cannot be less than zero")
	protected Integer estimatedTime;

	@Valid
	@NotNull(message = "'tags' Cannot be null")
	@CannotContainNull(message = "'tags' Cannot contain null")
	@CannotContainEmpty(message = "'tags' Cannot contain empty values")
	protected Set<String> tags;

	@NotNull(message = "'owner' Cannot be null")
	@NotEmpty(message = "'owner' Cannot be empty")
	protected String owner;

	@NotNull(message = "'shareableWithOtherApps' Cannot be null")
	protected Boolean shareableWithOtherApps;

	public TextDisplay getPrompt() {
		return prompt;
	}

	public void setPrompt(TextDisplay prompt) {
		this.prompt = prompt;
	}

	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	public Integer getMarks() {
		return marks;
	}

	public void setMarks(Integer marks) {
		this.marks = marks;
	}

	public Integer getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(Integer estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Boolean isShareableWithOtherApps() {
		return shareableWithOtherApps;
	}

	public void setShareableWithOtherApps(Boolean shareableWithOtherApps) {
		this.shareableWithOtherApps = shareableWithOtherApps;
	}

}
