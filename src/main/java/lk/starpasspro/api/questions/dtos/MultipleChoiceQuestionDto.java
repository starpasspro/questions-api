package lk.starpasspro.api.questions.dtos;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lk.starpasspro.api.questions.models.Option;
import lk.starpasspro.api.questions.models.TextDisplay;
import lk.starpasspro.api.questions.validate.CannotContainEmpty;
import lk.starpasspro.api.questions.validate.CannotContainNull;

public class MultipleChoiceQuestionDto extends BaseQuestionDto {

	@Valid
	@NotNull(message = "'options' Cannot be null")
	@CannotContainNull(message = "'options' Cannot contain null")
	@CannotContainEmpty(message = "'options' Cannot contain empty values")
	private Set<Option> options;

	@NotNull(message = "'correctAnswer' Cannot be null")
	@Min(value = 0, message = "'correctAnswer' Cannot be less than zero")
	private Integer correctAnswer;

	@Valid
	@NotNull(message = "'explanation' Cannot be null")
	private TextDisplay explanation;

	public Set<Option> getOptions() {
		return options;
	}

	public void setOptions(Set<Option> options) {
		this.options = options;
	}

	public Integer getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(Integer correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public TextDisplay getExplanation() {
		return explanation;
	}

	public void setExplanation(TextDisplay explanation) {
		this.explanation = explanation;
	}

}
