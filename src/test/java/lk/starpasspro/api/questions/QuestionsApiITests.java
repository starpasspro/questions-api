package lk.starpasspro.api.questions;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lk.starpasspro.api.questions.dtos.MatchingQuestionDto;
import lk.starpasspro.api.questions.dtos.MatchingQuestionOutputDto;
import lk.starpasspro.api.questions.dtos.MultipleChoiceQuestionDto;
import lk.starpasspro.api.questions.dtos.MultipleChoiceQuestionOutputDto;
import lk.starpasspro.api.questions.dtos.QuestionCardDto;
import lk.starpasspro.api.questions.dtos.QuestionCardOutputDto;
import lk.starpasspro.api.questions.dtos.ShortAnswerQuestionDto;
import lk.starpasspro.api.questions.dtos.ShortAnswerQuestionOutputDto;
import lk.starpasspro.api.questions.models.AccountCredential;
import lk.starpasspro.api.questions.models.LeftOption;
import lk.starpasspro.api.questions.models.MatchingOption;
import lk.starpasspro.api.questions.models.Option;
import lk.starpasspro.api.questions.models.QuestionType;
import lk.starpasspro.api.questions.models.TextDisplay;
import lk.starpasspro.api.questions.repositories.AccountCredentialsRepository;
import lk.starpasspro.api.questions.repositories.QuestionRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QuestionsApiApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class QuestionsApiITests {

	@Value("${local.server.port}")
	private int port;

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.token.prefix}")
	private String tokenPrefix;

	@Value("${jwt.header}")
	private String headerString;

	@Autowired
	private AccountCredentialsRepository accountCredentialsRepository;

	@Autowired
	private QuestionRepository questionRepository;

	private String securityToken;

	@Before
	public void setUp() {
		RestAssured.port = port;

		accountCredentialsRepository.deleteAll();
		questionRepository.deleteAll();

		AccountCredential accountCredential = new AccountCredential();
		accountCredential.setUsername("test_user");
		accountCredential.setPassword("test_user");

		accountCredentialsRepository.save(accountCredential);

		securityToken = Jwts.builder().setSubject("test_user").signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	@After
	public void cleanUp() {
		accountCredentialsRepository.deleteAll();
		questionRepository.deleteAll();
	}

	/*
	 *
	 * Start Multiple choice question save tests
	 *
	 */
	@Test
	public void testSaveMultupleChoiceQuestion() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK).body("id", notNullValue())
				.body("createdAt", notNullValue()).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(multipleChoiceQuestion.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(multipleChoiceQuestion.getPrompt().getImageValue()))
				.body("type", is(multipleChoiceQuestion.getType().name()))
				.body("marks", is(multipleChoiceQuestion.getMarks()))
				.body("estimatedTime", is(multipleChoiceQuestion.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(multipleChoiceQuestion.getTags().stream().findFirst().get()))
				.body("owner", is(multipleChoiceQuestion.getOwner())).body("shareableWithOtherApps", is(true))
				.body("options.size()", is(4)).body("correctAnswer", is(multipleChoiceQuestion.getCorrectAnswer()))
				.body("explanation.textValue", is(multipleChoiceQuestion.getExplanation().getTextValue()))
				.body("explanation.imageValue", is(multipleChoiceQuestion.getExplanation().getImageValue()));
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullPrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullTextPrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullImagePrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullType() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithInvalidType() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_HOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullMarks() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithInvaidMarks() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullEstimatedTime() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithInvaidEstimatedTime() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullAsTag() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullTagValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithEmptyTagValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOwner() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithEmptyOwner() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullShareable() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithInvalidShareable() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullAsOptions() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[null,{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithEmptyOptionValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[\"\",{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithEmptyOptionIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":null,\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithEmptyOptionLabelValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionLabelValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":null,\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionPromptValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":null},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionPromptTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullOptionPromptImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":null}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullCorrectAnswer() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setCorrectAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithInvalidCorrectAnswer() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setCorrectAnswer(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullExplanation() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();
		multipleChoiceQuestion.setExplanation(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullExplanationTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMultupleChoiceQuestionWithNullExplanationImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":null}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Multiple choice question save tests
	 *
	 */

	/*
	 *
	 * Start Multiple choice question update tests
	 *
	 */
	@Test
	public void testUpdateMultupleChoiceQuestion() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		multipleChoiceQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(multipleChoiceQuestionOutput.getId()))
				.body("createdAt", is(multipleChoiceQuestionOutput.getCreatedAt())).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(updatedPrompt.getTextValue()))
				.body("prompt.imageValue", is(updatedPrompt.getImageValue()))
				.body("type", is(multipleChoiceQuestionOutput.getType().name()))
				.body("marks", is(multipleChoiceQuestionOutput.getMarks()))
				.body("estimatedTime", is(multipleChoiceQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(multipleChoiceQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(multipleChoiceQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("options.size()", is(4))
				.body("correctAnswer", is(multipleChoiceQuestionOutput.getCorrectAnswer()))
				.body("explanation.textValue", is(multipleChoiceQuestionOutput.getExplanation().getTextValue()))
				.body("explanation.imageValue", is(multipleChoiceQuestionOutput.getExplanation().getImageValue()));
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvalidId() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		multipleChoiceQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/abc").then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullPrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullTextPrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullImagePrompt() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullType() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvalidType() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_HOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullMarks() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvaidMarks() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullEstimatedTime() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvaidEstimatedTime() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullAsTag() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullTagValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithEmptyTagValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOwner() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithEmptyOwner() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullShareable() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvalidShareable() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullAsOptions() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[null,{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithEmptyOptionValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[\"\",{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithEmptyOptionIdValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionIdValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":null,\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithEmptyOptionLabelValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionLabelValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":null,\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionPromptValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":null},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionPromptTextValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullOptionPromptImageValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":null}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullCorrectAnswer() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setCorrectAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithInvalidCorrectAnswer() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setCorrectAnswer(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullExplanation() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		multipleChoiceQuestion.setExplanation(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(multipleChoiceQuestion)
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullExplanationTextValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMultupleChoiceQuestionWithNullExplanationImageValue() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MULTIPLE_CHOICE\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"options\":[{\"id\":\"4\",\"label\":\"D\",\"prompt\":{\"textValue\":\"Matara\",\"imageValue\":\"\"}},{\"id\":\"1\",\"label\":\"A\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"2\",\"label\":\"B\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}},{\"id\":\"3\",\"label\":\"C\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}}],\"correctAnswer\":0,\"explanation\":{\"textValue\":\"This is explanation\",\"imageValue\":null}}")
				.put("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Multiple choice question update tests
	 *
	 */

	/*
	 *
	 * Start Short Answer question save tests
	 *
	 */

	@Test
	public void testSaveShortAnswerQuestion() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK).body("id", notNullValue())
				.body("createdAt", notNullValue()).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(shortAnswerQuestion.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(shortAnswerQuestion.getPrompt().getImageValue()))
				.body("type", is(shortAnswerQuestion.getType().name()))
				.body("marks", is(shortAnswerQuestion.getMarks()))
				.body("estimatedTime", is(shortAnswerQuestion.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(shortAnswerQuestion.getTags().stream().findFirst().get()))
				.body("owner", is(shortAnswerQuestion.getOwner())).body("shareableWithOtherApps", is(true))
				.body("sampleCorrectAnswer.textValue", is(shortAnswerQuestion.getSampleCorrectAnswer().getTextValue()))
				.body("sampleCorrectAnswer.imageValue",
						is(shortAnswerQuestion.getSampleCorrectAnswer().getImageValue()));
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullPrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullTextPrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullImagePrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullType() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithInvalidType() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ASWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullMarks() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithInvaidMarks() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullEstimatedTime() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithInvaidEstimatedTime() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullAsTag() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullTagValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithEmptyTagValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullOwner() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithEmptyOwner() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullShareable() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithInvalidShareable() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullSampleCorrectAnswer() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();
		shortAnswerQuestion.setSampleCorrectAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullSampleCorrectAnswerTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveShortAnswerQuestionWithNullSampleCorrectAnswerImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":null}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Short Answer question save tests
	 *
	 */

	/*
	 *
	 * Start Short Answer question update tests
	 *
	 */

	@Test
	public void testUpdateShortAnswerQuestion() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		shortAnswerQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(shortAnswerQuestionOutput.getId()))
				.body("createdAt", is(shortAnswerQuestionOutput.getCreatedAt())).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(updatedPrompt.getTextValue()))
				.body("prompt.imageValue", is(updatedPrompt.getImageValue()))
				.body("type", is(shortAnswerQuestionOutput.getType().name()))
				.body("marks", is(shortAnswerQuestionOutput.getMarks()))
				.body("estimatedTime", is(shortAnswerQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(shortAnswerQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(shortAnswerQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("sampleCorrectAnswer.textValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getTextValue()))
				.body("sampleCorrectAnswer.imageValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getImageValue()));
	}

	@Test
	public void testUpdateShortAnswerQuestionWithInvalidId() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		shortAnswerQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/abc").then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullPrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullTextPrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullImagePrompt() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullType() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithInvalidType() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ASWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullMarks() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithInvaidMarks() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullEstimatedTime() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithInvaidEstimatedTime() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullAsTag() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullTagValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithEmptyTagValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullOwner() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithEmptyOwner() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullShareable() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithInvalidShareable() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullSampleCorrectAnswer() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		shortAnswerQuestion.setSampleCorrectAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(shortAnswerQuestion)
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullSampleCorrectAnswerTextValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateShortAnswerQuestionWithNullSampleCorrectAnswerImageValue() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":null}}")
				.put("/api/questions/" + shortAnswerQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Short Answer question update tests
	 *
	 */

	/*
	 *
	 * Start Question Card save tests
	 *
	 */

	@Test
	public void testSaveQuestionCard() {
		QuestionCardDto questionCard = createQuestionCard();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK).body("id", notNullValue())
				.body("createdAt", notNullValue()).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(questionCard.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(questionCard.getPrompt().getImageValue()))
				.body("type", is(questionCard.getType().name())).body("marks", is(questionCard.getMarks()))
				.body("estimatedTime", is(questionCard.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(questionCard.getTags().stream().findFirst().get()))
				.body("owner", is(questionCard.getOwner())).body("shareableWithOtherApps", is(true))
				.body("answer.textValue", is(questionCard.getAnswer().getTextValue()))
				.body("answer.imageValue", is(questionCard.getAnswer().getImageValue()));
	}

	@Test
	public void testSaveQuestionCardWithNullPrompt() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullTextPrompt() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullImagePrompt() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullType() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithInvalidType() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CRD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"answer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullMarks() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithInvaidMarks() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullEstimatedTime() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithInvaidEstimatedTime() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullAsTag() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullTagValue() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithEmptyTagValue() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullOwner() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithEmptyOwner() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullShareable() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithInvalidShareable() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CARD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"answer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullAnswer() {
		QuestionCardDto questionCard = createQuestionCard();
		questionCard.setAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullAnswerTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CARD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"answer\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveQuestionCardWithNullAnswerImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CARD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"answer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":null}}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Question Card save tests
	 *
	 */

	/*
	 *
	 * Start Question Card question update tests
	 *
	 */

	@Test
	public void testUpdateQuestionCardQuestion() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		questionCard.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(questionCardOutput.getId())).body("createdAt", is(questionCardOutput.getCreatedAt()))
				.body("updatedAt", notNullValue()).body("prompt.textValue", is(updatedPrompt.getTextValue()))
				.body("prompt.imageValue", is(updatedPrompt.getImageValue()))
				.body("type", is(questionCardOutput.getType().name())).body("marks", is(questionCardOutput.getMarks()))
				.body("estimatedTime", is(questionCardOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(questionCardOutput.getTags().stream().findFirst().get()))
				.body("owner", is(questionCardOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("answer.textValue", is(questionCardOutput.getAnswer().getTextValue()))
				.body("answer.imageValue", is(questionCardOutput.getAnswer().getImageValue()));
	}

	@Test
	public void testUpdateQuestionCardQuestionWithInvalidId() {
		QuestionCardDto questionCard = createQuestionCard();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		questionCard.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/abc").then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testUpdateQuestionCardWithNullPrompt() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullTextPrompt() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullImagePrompt() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullType() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithInvalidType() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CRD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullMarks() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithInvaidMarks() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullEstimatedTime() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithInvaidEstimatedTime() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullAsTag() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullTagValue() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithEmptyTagValue() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullOwner() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithEmptyOwner() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullShareable() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithInvalidShareable() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"SHORT_ANSWER\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tre,\"sampleCorrectAnswer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":\"\"}}")
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullAnswer() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		questionCard.setAnswer(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateSQuestionCardnWithNullAnswerTextValue() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CARD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"answer\":{\"textValue\":null,\"imageValue\":\"\"}}")
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateQuestionCardWithNullAnswerImageValue() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"What is the capital of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"QUESTION_CARD\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"answer\":{\"textValue\":\"Colombo is the answer\",\"imageValue\":null}}")
				.put("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Question Card update tests
	 *
	 */

	/*
	 *
	 * Start Matching Question save tests
	 *
	 */

	@Test
	public void testSaveMatchingQuestion() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK).body("id", notNullValue())
				.body("createdAt", notNullValue()).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(matchingQuestion.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(matchingQuestion.getPrompt().getImageValue()))
				.body("type", is(matchingQuestion.getType().name())).body("marks", is(matchingQuestion.getMarks()))
				.body("estimatedTime", is(matchingQuestion.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(matchingQuestion.getTags().stream().findFirst().get()))
				.body("owner", is(matchingQuestion.getOwner())).body("shareableWithOtherApps", is(true))
				.body("leftOptions.size()", is(3)).body("rightOptions.size()", is(3));
	}

	@Test
	public void testSaveMatchingQuestionWithNullPrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullTextPrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullImagePrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullType() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithInvalidType() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHIG\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullMarks() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithInvaidMarks() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullEstimatedTime() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithInvaidEstimatedTime() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullAsTag() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullTagValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyTagValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullOwner() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyOwner() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullShareable() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithInvalidShareable() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tue,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullRightOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setRightOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullValueForRightOptions() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[null,{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyValueForRightOptions() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[\"\",{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullRightOptionsIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":null,\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyRightOptionsIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullRightOptionsPromptValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":null},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithRightOptionsPromptNullTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithRightOptionsNullImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":null}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullLeftOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();
		matchingQuestion.setLeftOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullValueForLeftOptions() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[null,{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyValueForLeftOptions() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[\"\",{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullLeftOptionsIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":null,\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithEmptyLeftOptionsIdValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithNullLeftOptionsPromptValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":null,\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithLeftOptionsPromptNullTextValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testSaveMatchingQuestionWithLeftOptionsNullImageValue() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":null},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.post("/api/questions/").then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Matching Question save tests
	 *
	 */

	/*
	 *
	 * Start Matching Question update tests
	 *
	 */

	@Test
	public void testUpdateMatchingQuestion() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		matchingQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(matchingQuestionOutput.getId()))
				.body("createdAt", is(matchingQuestionOutput.getCreatedAt())).body("updatedAt", notNullValue())
				.body("prompt.textValue", is(updatedPrompt.getTextValue()))
				.body("prompt.imageValue", is(updatedPrompt.getImageValue()))
				.body("type", is(matchingQuestionOutput.getType().name()))
				.body("marks", is(matchingQuestion.getMarks()))
				.body("estimatedTime", is(matchingQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(matchingQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(matchingQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("leftOptions.size()", is(3)).body("rightOptions.size()", is(3));
	}

	@Test
	public void testUpdateMatchingQuestionWithInvalidId() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		TextDisplay updatedPrompt = new TextDisplay();
		updatedPrompt.setTextValue("What is the largest city in Sri Lanka?");
		updatedPrompt.setImageValue("");
		matchingQuestion.setPrompt(updatedPrompt);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/abc").then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullPrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setPrompt(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullTextPrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.getPrompt().setTextValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullImagePrompt() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.getPrompt().setImageValue(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullType() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setType(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithInvalidType() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHIG\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullMarks() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setMarks(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithInvaidMarks() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setMarks(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullEstimatedTime() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setEstimatedTime(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithInvaidEstimatedTime() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setEstimatedTime(-1);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullAsTag() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setTags(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullTagValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", null)));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyTagValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01", "")));

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullOwner() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setOwner(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyOwner() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setOwner("");

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullShareable() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setShareableWithOtherApps(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithInvalidShareable() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":tue,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullRightOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setRightOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullValueForRightOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[null,{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyValueForRightOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[\"\",{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullRightOptionsIdValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":null,\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyRightOptionsIdValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullRightOptionsPromptValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":null},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithRightOptionsPromptNullTextValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithRightOptionsNullImageValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":null}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullLeftOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		matchingQuestion.setLeftOptions(null);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullValueForLeftOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[null,{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyValueForLeftOptions() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[\"\",{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullLeftOptionsIdValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":null,\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithEmptyLeftOptionsIdValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithNullLeftOptionsPromptValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":null,\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithLeftOptionsPromptNullTextValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":null,\"imageValue\":\"\"},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	@Test
	public void testUpdateMatchingQuestionWithLeftOptionsNullImageValue() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);
		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.content(
						"{\"prompt\":{\"textValue\":\"Match the capital of the states of Sri Lanka?\",\"imageValue\":\"http://starpasspro.lk/assets/images/logo.png\"},\"type\":\"MATCHING\",\"marks\":10,\"estimatedTime\":2,\"tags\":[\"Tag 01\"],\"owner\":\"Starpasspro\",\"shareableWithOtherApps\":true,\"leftOptions\":[{\"id\":\"1\",\"prompt\":{\"textValue\":\"Southern\",\"imageValue\":null},\"rightOptionIndex\":1},{\"id\":\"2\",\"prompt\":{\"textValue\":\"Western\",\"imageValue\":\"\"},\"rightOptionIndex\":0},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Central\",\"imageValue\":\"\"},\"rightOptionIndex\":2}],\"rightOptions\":[{\"id\":\"2\",\"prompt\":{\"textValue\":\"Galle\",\"imageValue\":\"\"}},{\"id\":\"1\",\"prompt\":{\"textValue\":\"Colombo\",\"imageValue\":\"\"}},{\"id\":\"3\",\"prompt\":{\"textValue\":\"Kandy\",\"imageValue\":\"\"}}]}")
				.put("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_BAD_REQUEST);
	}

	/*
	 *
	 * Ends Matching Question update tests
	 *
	 */

	/*
	 *
	 * Starts Find Question Tests
	 *
	 */

	@Test
	public void testFindMultupleChoiceQuestion() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/questions/" + multipleChoiceQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(multipleChoiceQuestionOutput.getId()))
				.body("createdAt", is(multipleChoiceQuestionOutput.getCreatedAt()))
				.body("updatedAt", is(multipleChoiceQuestionOutput.getUpdatedAt()))
				.body("prompt.textValue", is(multipleChoiceQuestionOutput.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(multipleChoiceQuestionOutput.getPrompt().getImageValue()))
				.body("type", is(multipleChoiceQuestionOutput.getType().name()))
				.body("marks", is(multipleChoiceQuestionOutput.getMarks()))
				.body("estimatedTime", is(multipleChoiceQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(multipleChoiceQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(multipleChoiceQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("options.size()", is(4))
				.body("correctAnswer", is(multipleChoiceQuestionOutput.getCorrectAnswer()))
				.body("explanation.textValue", is(multipleChoiceQuestionOutput.getExplanation().getTextValue()))
				.body("explanation.imageValue", is(multipleChoiceQuestionOutput.getExplanation().getImageValue()));
	}

	@Test
	public void testFindShortAnswerQuestion() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/questions/" + shortAnswerQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(shortAnswerQuestionOutput.getId()))
				.body("createdAt", is(shortAnswerQuestionOutput.getCreatedAt()))
				.body("updatedAt", is(shortAnswerQuestionOutput.getUpdatedAt()))
				.body("prompt.textValue", is(shortAnswerQuestionOutput.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(shortAnswerQuestionOutput.getPrompt().getImageValue()))
				.body("type", is(shortAnswerQuestionOutput.getType().name()))
				.body("marks", is(shortAnswerQuestionOutput.getMarks()))
				.body("estimatedTime", is(shortAnswerQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(shortAnswerQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(shortAnswerQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("sampleCorrectAnswer.textValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getTextValue()))
				.body("sampleCorrectAnswer.imageValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getImageValue()));
	}

	@Test
	public void testFindQuestionCardQuestion() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/questions/" + questionCardOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(questionCardOutput.getId())).body("createdAt", is(questionCardOutput.getCreatedAt()))
				.body("updatedAt", is(questionCardOutput.getUpdatedAt()))
				.body("prompt.textValue", is(questionCardOutput.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(questionCardOutput.getPrompt().getImageValue()))
				.body("type", is(questionCardOutput.getType().name())).body("marks", is(questionCardOutput.getMarks()))
				.body("estimatedTime", is(questionCardOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(questionCardOutput.getTags().stream().findFirst().get()))
				.body("owner", is(questionCardOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("answer.textValue", is(questionCardOutput.getAnswer().getTextValue()))
				.body("answer.imageValue", is(questionCardOutput.getAnswer().getImageValue()));
	}

	@Test
	public void testFindMatchingQuestion() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/questions/" + matchingQuestionOutput.getId()).then().statusCode(HttpStatus.SC_OK)
				.body("id", is(matchingQuestionOutput.getId()))
				.body("createdAt", is(matchingQuestionOutput.getCreatedAt()))
				.body("updatedAt", is(matchingQuestionOutput.getUpdatedAt()))
				.body("prompt.textValue", is(matchingQuestionOutput.getPrompt().getTextValue()))
				.body("prompt.imageValue", is(matchingQuestionOutput.getPrompt().getImageValue()))
				.body("type", is(matchingQuestionOutput.getType().name()))
				.body("marks", is(matchingQuestion.getMarks()))
				.body("estimatedTime", is(matchingQuestionOutput.getEstimatedTime())).body("tags.size()", is(1))
				.body("tags[0]", is(matchingQuestionOutput.getTags().stream().findFirst().get()))
				.body("owner", is(matchingQuestionOutput.getOwner())).body("shareableWithOtherApps", is(true))
				.body("leftOptions.size()", is(3)).body("rightOptions.size()", is(3));
	}

	@Test
	public void testFindNonExistingQuestion() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/abc")
				.then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	/*
	 *
	 * Ends Find Question Tests
	 *
	 */

	/*
	 *
	 * Starts Find All Questions and Find By Ids Tests
	 *
	 */

	@Test
	public void testFindAllQuestionsWithMultipleChoice() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(1))
				.body("[0].id", is(multipleChoiceQuestionOutput.getId()))
				.body("[0].createdAt", is(multipleChoiceQuestionOutput.getCreatedAt()))
				.body("[0].updatedAt", is(multipleChoiceQuestionOutput.getUpdatedAt()))
				.body("[0].prompt.textValue", is(multipleChoiceQuestionOutput.getPrompt().getTextValue()))
				.body("[0].prompt.imageValue", is(multipleChoiceQuestionOutput.getPrompt().getImageValue()))
				.body("[0].type", is(multipleChoiceQuestionOutput.getType().name()))
				.body("[0].marks", is(multipleChoiceQuestionOutput.getMarks()))
				.body("[0].estimatedTime", is(multipleChoiceQuestionOutput.getEstimatedTime()))
				.body("[0].tags.size()", is(1))
				.body("[0].tags[0]", is(multipleChoiceQuestionOutput.getTags().stream().findFirst().get()))
				.body("[0].owner", is(multipleChoiceQuestionOutput.getOwner()))
				.body("[0].shareableWithOtherApps", is(true)).body("[0].options.size()", is(4))
				.body("[0].correctAnswer", is(multipleChoiceQuestionOutput.getCorrectAnswer()))
				.body("[0].explanation.textValue", is(multipleChoiceQuestionOutput.getExplanation().getTextValue()))
				.body("[0].explanation.imageValue", is(multipleChoiceQuestionOutput.getExplanation().getImageValue()));
	}

	@Test
	public void testFindAllQuestionsWithShortAnswer() {
		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(1))
				.body("[0].id", is(shortAnswerQuestionOutput.getId()))
				.body("[0].createdAt", is(shortAnswerQuestionOutput.getCreatedAt()))
				.body("[0].updatedAt", is(shortAnswerQuestionOutput.getUpdatedAt()))
				.body("[0].prompt.textValue", is(shortAnswerQuestionOutput.getPrompt().getTextValue()))
				.body("[0].prompt.imageValue", is(shortAnswerQuestionOutput.getPrompt().getImageValue()))
				.body("[0].type", is(shortAnswerQuestionOutput.getType().name()))
				.body("[0].marks", is(shortAnswerQuestionOutput.getMarks()))
				.body("[0].estimatedTime", is(shortAnswerQuestionOutput.getEstimatedTime()))
				.body("[0].tags.size()", is(1))
				.body("[0].tags[0]", is(shortAnswerQuestionOutput.getTags().stream().findFirst().get()))
				.body("[0].owner", is(shortAnswerQuestionOutput.getOwner()))
				.body("[0].shareableWithOtherApps", is(true))
				.body("[0].sampleCorrectAnswer.textValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getTextValue()))
				.body("[0].sampleCorrectAnswer.imageValue",
						is(shortAnswerQuestionOutput.getSampleCorrectAnswer().getImageValue()));
	}

	@Test
	public void testFindAllQuestionsWithQuestionCard() {
		QuestionCardDto questionCard = createQuestionCard();

		QuestionCardOutputDto questionCardOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(questionCard).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(QuestionCardOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(1))
				.body("[0].id", is(questionCardOutput.getId()))
				.body("[0].createdAt", is(questionCardOutput.getCreatedAt()))
				.body("[0].updatedAt", is(questionCardOutput.getUpdatedAt()))
				.body("[0].prompt.textValue", is(questionCardOutput.getPrompt().getTextValue()))
				.body("[0].prompt.imageValue", is(questionCardOutput.getPrompt().getImageValue()))
				.body("[0].type", is(questionCardOutput.getType().name()))
				.body("[0].marks", is(questionCardOutput.getMarks()))
				.body("[0].estimatedTime", is(questionCardOutput.getEstimatedTime())).body("[0].tags.size()", is(1))
				.body("[0].tags[0]", is(questionCardOutput.getTags().stream().findFirst().get()))
				.body("[0].owner", is(questionCardOutput.getOwner())).body("[0].shareableWithOtherApps", is(true))
				.body("[0].answer.textValue", is(questionCardOutput.getAnswer().getTextValue()))
				.body("[0].answer.imageValue", is(questionCardOutput.getAnswer().getImageValue()));
	}

	@Test
	public void testFindAllQuestionsWithMatching() {
		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		MatchingQuestionOutputDto matchingQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(matchingQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(MatchingQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(1))
				.body("[0].id", is(matchingQuestionOutput.getId()))
				.body("[0].createdAt", is(matchingQuestionOutput.getCreatedAt()))
				.body("[0].updatedAt", is(matchingQuestionOutput.getUpdatedAt()))
				.body("[0].prompt.textValue", is(matchingQuestionOutput.getPrompt().getTextValue()))
				.body("[0].prompt.imageValue", is(matchingQuestionOutput.getPrompt().getImageValue()))
				.body("[0].type", is(matchingQuestionOutput.getType().name()))
				.body("[0].marks", is(matchingQuestion.getMarks()))
				.body("[0].estimatedTime", is(matchingQuestionOutput.getEstimatedTime())).body("[0].tags.size()", is(1))
				.body("[0].tags[0]", is(matchingQuestionOutput.getTags().stream().findFirst().get()))
				.body("[0].owner", is(matchingQuestionOutput.getOwner())).body("[0].shareableWithOtherApps", is(true))
				.body("[0].leftOptions.size()", is(3)).body("[0].rightOptions.size()", is(3));
	}

	@Test
	public void testFindAllQuestionsWhenEmptyDB() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(0));
	}

	@Test
	public void testFindAllQuestionsWithProvidedIds() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);

		MatchingQuestionDto matchingQuestion = createMatchingQuestion();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(matchingQuestion)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		QuestionCardDto questionCard = createQuestionCard();

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().content(questionCard)
				.post("/api/questions/").then().statusCode(HttpStatus.SC_OK);

		ShortAnswerQuestionDto shortAnswerQuestion = createShortAnswerQuestion();

		ShortAnswerQuestionOutputDto shortAnswerQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(shortAnswerQuestion).post("/api/questions/").then()
				.statusCode(HttpStatus.SC_OK).extract().as(ShortAnswerQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.get("/api/questions/all?id=" + shortAnswerQuestionOutput.getId() + "&id="
						+ multipleChoiceQuestionOutput.getId())
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(2))
				.body("id", hasItems(multipleChoiceQuestionOutput.getId(), shortAnswerQuestionOutput.getId()));
	}

	/*
	 *
	 * Ends Find All Questions and Find By Ids Tests
	 *
	 */

	/*
	 *
	 * Starts Delete Question Tests
	 *
	 */

	@Test
	public void testDeleteQuestion() {
		MultipleChoiceQuestionDto multipleChoiceQuestion = createMultipleChoiceQuestion();

		MultipleChoiceQuestionOutputDto multipleChoiceQuestionOutput = given().contentType(ContentType.JSON)
				.header(headerString, securityToken).when().content(multipleChoiceQuestion).post("/api/questions/")
				.then().statusCode(HttpStatus.SC_OK).extract().as(MultipleChoiceQuestionOutputDto.class);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when()
				.delete("/api/questions/" + multipleChoiceQuestionOutput.getId()).then()
				.statusCode(HttpStatus.SC_OK);

		given().contentType(ContentType.JSON).header(headerString, securityToken).when().get("/api/questions/all")
				.then().statusCode(HttpStatus.SC_OK).body("$", Matchers.hasSize(0));
	}

	@Test
	public void testDeleteNonExistingQuestion() {
		given().contentType(ContentType.JSON).header(headerString, securityToken).when().delete("/api/questions/abc")
				.then().statusCode(HttpStatus.SC_NOT_FOUND);
	}

	/*
	 *
	 * Ends Delete Question Tests
	 *
	 */

	private MultipleChoiceQuestionDto createMultipleChoiceQuestion() {

		MultipleChoiceQuestionDto multipleChoiceQuestion = new MultipleChoiceQuestionDto();

		TextDisplay prompt = new TextDisplay();
		prompt.setTextValue("What is the capital of Sri Lanka?");
		prompt.setImageValue("http://starpasspro.lk/assets/images/logo.png");
		multipleChoiceQuestion.setPrompt(prompt);

		multipleChoiceQuestion.setType(QuestionType.MULTIPLE_CHOICE);
		multipleChoiceQuestion.setMarks(10);
		multipleChoiceQuestion.setEstimatedTime(2);
		multipleChoiceQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01")));
		multipleChoiceQuestion.setOwner("Starpasspro");
		multipleChoiceQuestion.setShareableWithOtherApps(true);

		Option option1 = new Option();
		option1.setId("1");
		option1.setLabel("A");
		TextDisplay option1Prompt = new TextDisplay();
		option1Prompt.setTextValue("Colombo");
		option1Prompt.setImageValue("");
		option1.setPrompt(option1Prompt);

		Option option2 = new Option();
		option2.setId("2");
		option2.setLabel("B");
		TextDisplay option2Prompt = new TextDisplay();
		option2Prompt.setTextValue("Kandy");
		option2Prompt.setImageValue("");
		option2.setPrompt(option2Prompt);

		Option option3 = new Option();
		option3.setId("3");
		option3.setLabel("C");
		TextDisplay option3Prompt = new TextDisplay();
		option3Prompt.setTextValue("Galle");
		option3Prompt.setImageValue("");
		option3.setPrompt(option3Prompt);

		Option option4 = new Option();
		option4.setId("4");
		option4.setLabel("D");
		TextDisplay option4Prompt = new TextDisplay();
		option4Prompt.setTextValue("Matara");
		option4Prompt.setImageValue("");
		option4.setPrompt(option4Prompt);

		multipleChoiceQuestion.setOptions(new HashSet<>(Arrays.asList(option1, option2, option3, option4)));
		multipleChoiceQuestion.setCorrectAnswer(0);

		TextDisplay explanation = new TextDisplay();
		explanation.setTextValue("This is explanation");
		explanation.setImageValue("");

		multipleChoiceQuestion.setExplanation(explanation);

		return multipleChoiceQuestion;

	}

	private ShortAnswerQuestionDto createShortAnswerQuestion() {

		ShortAnswerQuestionDto shortAnswerQuestion = new ShortAnswerQuestionDto();

		TextDisplay prompt = new TextDisplay();
		prompt.setTextValue("What is the capital of Sri Lanka?");
		prompt.setImageValue("http://starpasspro.lk/assets/images/logo.png");
		shortAnswerQuestion.setPrompt(prompt);

		shortAnswerQuestion.setType(QuestionType.SHORT_ANSWER);
		shortAnswerQuestion.setMarks(10);
		shortAnswerQuestion.setEstimatedTime(2);
		shortAnswerQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01")));
		shortAnswerQuestion.setOwner("Starpasspro");
		shortAnswerQuestion.setShareableWithOtherApps(true);

		TextDisplay sampleAnswer = new TextDisplay();
		sampleAnswer.setTextValue("Colombo is the answer");
		sampleAnswer.setImageValue("");

		shortAnswerQuestion.setSampleCorrectAnswer(sampleAnswer);

		return shortAnswerQuestion;

	}

	private QuestionCardDto createQuestionCard() {

		QuestionCardDto questionCard = new QuestionCardDto();

		TextDisplay prompt = new TextDisplay();
		prompt.setTextValue("What is the capital of Sri Lanka?");
		prompt.setImageValue("http://starpasspro.lk/assets/images/logo.png");
		questionCard.setPrompt(prompt);

		questionCard.setType(QuestionType.QUESTION_CARD);
		questionCard.setMarks(10);
		questionCard.setEstimatedTime(2);
		questionCard.setTags(new HashSet<>(Arrays.asList("Tag 01")));
		questionCard.setOwner("Starpasspro");
		questionCard.setShareableWithOtherApps(true);

		TextDisplay answer = new TextDisplay();
		answer.setTextValue("Colombo");
		answer.setImageValue("");

		questionCard.setAnswer(answer);

		return questionCard;

	}

	private MatchingQuestionDto createMatchingQuestion() {

		MatchingQuestionDto matchingQuestion = new MatchingQuestionDto();

		TextDisplay prompt = new TextDisplay();
		prompt.setTextValue("Match the capital of the states of Sri Lanka?");
		prompt.setImageValue("http://starpasspro.lk/assets/images/logo.png");
		matchingQuestion.setPrompt(prompt);

		matchingQuestion.setType(QuestionType.MATCHING);
		matchingQuestion.setMarks(10);
		matchingQuestion.setEstimatedTime(2);
		matchingQuestion.setTags(new HashSet<>(Arrays.asList("Tag 01")));
		matchingQuestion.setOwner("Starpasspro");
		matchingQuestion.setShareableWithOtherApps(true);

		LeftOption leftOption1 = new LeftOption();
		leftOption1.setId("1");
		leftOption1.setRightOptionIndex(1);
		TextDisplay leftOption1Prompt = new TextDisplay();
		leftOption1Prompt.setTextValue("Southern");
		leftOption1Prompt.setImageValue("");
		leftOption1.setPrompt(leftOption1Prompt);

		LeftOption leftOption2 = new LeftOption();
		leftOption2.setId("2");
		leftOption2.setRightOptionIndex(0);
		TextDisplay leftOption2Prompt = new TextDisplay();
		leftOption2Prompt.setTextValue("Western");
		leftOption2Prompt.setImageValue("");
		leftOption2.setPrompt(leftOption2Prompt);

		LeftOption leftOption3 = new LeftOption();
		leftOption3.setId("3");
		leftOption3.setRightOptionIndex(2);
		TextDisplay leftOption3Prompt = new TextDisplay();
		leftOption3Prompt.setTextValue("Central");
		leftOption3Prompt.setImageValue("");
		leftOption3.setPrompt(leftOption3Prompt);

		matchingQuestion.setLeftOptions(new HashSet<>(Arrays.asList(leftOption1, leftOption2, leftOption3)));

		MatchingOption rightOption1 = new MatchingOption();
		rightOption1.setId("1");
		TextDisplay rightOption1Prompt = new TextDisplay();
		rightOption1Prompt.setTextValue("Colombo");
		rightOption1Prompt.setImageValue("");
		rightOption1.setPrompt(rightOption1Prompt);

		MatchingOption rightOption2 = new MatchingOption();
		rightOption2.setId("2");
		TextDisplay rightOption2Prompt = new TextDisplay();
		rightOption2Prompt.setTextValue("Galle");
		rightOption2Prompt.setImageValue("");
		rightOption2.setPrompt(rightOption2Prompt);

		MatchingOption rightOption3 = new MatchingOption();
		rightOption3.setId("3");
		TextDisplay rightOption3Prompt = new TextDisplay();
		rightOption3Prompt.setTextValue("Kandy");
		rightOption3Prompt.setImageValue("");
		rightOption3.setPrompt(rightOption3Prompt);

		matchingQuestion.setRightOptions(new HashSet<>(Arrays.asList(rightOption1, rightOption2, rightOption3)));

		return matchingQuestion;

	}

}
